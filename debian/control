Source: r-cran-psychtools
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-psychtools
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-psychtools.git
Homepage: https://cran.r-project.org/package=psychTools
Standards-Version: 4.6.1
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-foreign,
               r-cran-psych
Testsuite: autopkgtest-pkg-r

Package: r-cran-psychtools
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R tools to accompany the 'r-cran-psych'
 This GNU R package provides Psychological Research Support functions,
 data sets, and vignettes for the 'psych' package. It contains several of
 the biggest data sets for the 'psych' package as well as one vignette. A
 few helper functions for file manipulation are included as well. For
 more information, see the <https://personality-project.org/r> web page.
